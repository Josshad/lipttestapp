//
//  CacheHex.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

protocol CacheHex {
    func cachedItemFor(_ id: String) -> URL?
    func cacheItemFor(_ id: String, tempFileURL: URL) -> URL?
}

class FileCacheHexImpl: CacheHex {
    private var cacheURL: URL? {
        fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first
    }
    
    private let fileManager: FileManager
    init(fileManager: FileManager = FileManager.default) {
        self.fileManager = fileManager
    }
    
    func cachedItemFor(_ id: String) -> URL? {
        if let destination = self.cacheURL?.appendingPathComponent(id),
            self.fileManager.fileExists(atPath: destination.path) {
            return destination
        }
        return nil
    }
    
    func cacheItemFor(_ id: String, tempFileURL: URL) -> URL? {
        if let destination = self.cacheURL?.appendingPathComponent(id) {
            do {
                try fileManager.moveItem(at: tempFileURL, to: destination)
                return destination
            } catch {
                return nil
            }
        }
        return nil
    }
    
}
