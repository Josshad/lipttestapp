//
//  CatsHex.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

protocol CatsHex {
    var catsModels: [CatModel] { get }
    func loadNextPage(_ completion: @escaping (Error?) -> Void)
    func downloadImage(for id: String, completion: @escaping (URL?, Error?) -> Void)
    func cancelDownloadImage(for id: String)
}

class CatsHexImpl: CatsHex {
    typealias Completion = (Error?) -> Void
    private var currentPage: Int = 0
    private let service: CatsService
    private let downloadService: DownloadService
    
    private var urlMap: [String: URL] = [:]
    private var tasksMap: [String: RequestHandle] = [:]
    private var downloadQueue = DispatchQueue(label: "cats.hex.download.serial")
    
    private let completionsMutex = NSLock()
    private var completions: [Completion] = []
    
    var catsModels: [CatModel] = []
    
    init(service: CatsService,
         downloadService: DownloadService) {
        self.service = service
        self.downloadService = downloadService
    }
    
    func loadNextPage(_ completion: @escaping Completion) {
        guard addCompletion(completion) else {
            return
        }
        service.fetchCats(page: currentPage) { [weak self] (models, error) in
            switch (models, error) {
            case (_, let error?):
                self?.callAndFlushCompletions(error)
            case (let models?, _):
                self?.currentPage += 1
                self?.addModels(models: models)
                self?.callAndFlushCompletions(nil)
            default: assertionFailure("Unsupported state")
            }
        }
    }
    
    func downloadImage(for id: String, completion: @escaping (URL?, Error?) -> Void) {
        guard let url = urlMap[id] else {
            return
        }
        downloadQueue.async { [weak self] in
            guard let self = self else { return }
            let handle = self.downloadService.downloadImage(url) { [weak self] (url, error) in
                completion(url, error)
                self?.removeTask(id: id)
            }
            self.tasksMap[id] = handle
        }
    }
    
    func cancelDownloadImage(for id: String) {
        downloadQueue.async { [weak self] in
            self?.tasksMap[id]?.cancel()
            self?.removeTask(id: id)
        }
    }
    
    private func removeTask(id: String) {
        downloadQueue.async { [weak self] in
            self?.tasksMap[id] = nil
        }
    }
    
    private func addModels(models: [CatModel]) {
        let models = models.filter({ urlMap[$0.id] == nil })
        models.forEach({ urlMap[$0.id] = $0.url })
        catsModels += models
    }
    
    private func addCompletion(_ completion: @escaping Completion) -> Bool {
        completionsMutex.lock()
        completions.append(completion)
        let result = completions.count == 1
        completionsMutex.unlock()
        return result
    }
    
    private func callAndFlushCompletions(_ error: Error?) {
        completionsMutex.lock()
        let tempCompletions = completions
        completions = []
        completionsMutex.unlock()
        tempCompletions.forEach({ $0(error) })
    }
}
