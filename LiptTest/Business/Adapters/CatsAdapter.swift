//
//  CatsAdapter.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

protocol CatsAdapter {
    var catsModels: [CatModel] { get }
    func loadNextPage(_ completion: @escaping (Error?) -> Void)
    func loadImageFor(id: String, completion: @escaping (URL?) -> Void)
    func cancelDownloadImage(id: String)
    func cachedImageURL(for id:String) -> URL?
}

struct CatsAdapterImpl: CatsAdapter {
    private let catsHex: CatsHex
    private let cacheHex: CacheHex
    
    init(catsHex: CatsHex,
         cacheHex: CacheHex) {
        self.catsHex = catsHex
        self.cacheHex = cacheHex
    }
    
    func loadNextPage(_ completion: @escaping (Error?) -> Void) {
        catsHex.loadNextPage(completion)
    }
    
    var catsModels: [CatModel] {
        catsHex.catsModels
    }
    
    func cachedImageURL(for id:String) -> URL? {
        cacheHex.cachedItemFor(id)
    }
    
    func loadImageFor(id: String, completion: @escaping (URL?) -> Void) {
        if let url = cachedImageURL(for: id) {
            completion(url)
        } else {
            catsHex.downloadImage(for: id) { [cacheHex] (url, error) in
                switch (url, error) {
                case (let url?, _):
                    if let saved = cacheHex.cachedItemFor(id) {
                        completion(saved)
                    } else {
                        completion(cacheHex.cacheItemFor(id, tempFileURL: url))
                    }
                default: ()    
                }
            }
        }
    }
    
    func cancelDownloadImage(id: String) {
        catsHex.cancelDownloadImage(for: id)
    }
}
