//
//  CatsService.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

protocol CatsService {
    func fetchCats(page: Int, completion: @escaping ([CatModel]?, Error?) -> Void)
}

struct CatsServiceImpl: CatsService {
    private enum Paths {
        static let fetchPage = "images/search?limit=20&order=DESC&page=%li"
    }
    
    private let httpClient: HTTPClient
    init(httpClient: HTTPClient) {
        self.httpClient = httpClient
    }
    
    func fetchCats(page: Int, completion: @escaping ([CatModel]?, Error?) -> Void) {
        let request = Request(path: String(format: Paths.fetchPage, page))
        httpClient.getRequest(request: request) { (data: [CatModel]?, error) in
            switch (data, error) {
            case (_, let error?): completion(nil, error)
            case (nil, _): completion(nil, NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse))
            case (let list?, nil):
                completion(list, nil)
            }
        }
    }
}


