//
//  DownloadService.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

struct DownloadService {
    private let httpClient: HTTPDownloadClient
    init(httpClient: HTTPDownloadClient) {
        self.httpClient = httpClient
    }
    
    func downloadImage(_ url: URL, completion: @escaping (URL?, Error?) -> Void) -> RequestHandle {
        httpClient.download(url: url, completion: completion)
    }
    
}
