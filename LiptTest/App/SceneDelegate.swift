//
//  SceneDelegate.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var metaAssembly = MetaAssembly()
    
    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let catsAssembly = metaAssembly.catsAssembly()
        window = UIWindow(windowScene: windowScene)
        window?.rootViewController = catsAssembly.instantiateRootViewController()
        window?.makeKeyAndVisible()
    }
}

