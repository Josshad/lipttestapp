//
//  CatsCell.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

class CatsCell: UICollectionViewCell {
    
    private enum Consts {
        static let fontSize: CGFloat = 15
        static let imageHeight: CGFloat = 50
        static let cellHeight: CGFloat = 64
        static let sideInset: CGFloat = 12
        static let imageToLabelSpace: CGFloat = 16
    }
    
    static let prefferedHeight: CGFloat = Consts.cellHeight
    
    private let imageView = UIImageView()
    private let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageView.backgroundColor = .darkGray
        imageView.contentMode = .scaleAspectFit
        addSubview(imageView)
        label.font = .systemFont(ofSize: Consts.fontSize)
        addSubview(label)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unavailable")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = CGRect(x: Consts.sideInset, y: bounds.midY - Consts.imageHeight / 2,
                                 width: Consts.imageHeight * ratio, height: Consts.imageHeight)
        let labelOrigin = imageView.frame.maxX + Consts.imageToLabelSpace
        let labelWidth = bounds.width - labelOrigin
        let labelSize = label.sizeThatFits(CGSize(width: labelWidth, height: bounds.height))
        label.frame = CGRect(x: labelOrigin, y: bounds.midY - labelSize.height / 2,
                             width: labelSize.width, height: labelSize.height)
    }
    
    var ratio: CGFloat = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    
    var text: String? {
        get {
            label.text
        }
        set {
            label.text = newValue
            setNeedsLayout()
        }
    }
    
    var image: UIImage? {
        get {
            imageView.image
        }
        set {
            imageView.backgroundColor = newValue != nil ? .white : .darkGray
            imageView.image = newValue
        }
    }
}
