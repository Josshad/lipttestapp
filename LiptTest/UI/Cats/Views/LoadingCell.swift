//
//  LoadingCell.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

class LoadingCell: UICollectionViewCell {
    private let loadingIndicator = UIActivityIndicatorView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadingIndicator.style = .medium
        loadingIndicator.tintColor = .gray
        loadingIndicator.startAnimating()
        addSubview(loadingIndicator)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unavailable")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        loadingIndicator.startAnimating()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        loadingIndicator.sizeToFit()
        loadingIndicator.center = CGPoint(x: bounds.midX, y: bounds.midY)
    }
}
