//
//  DetailsView.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

class DetailsView: UIScrollView {
    private enum Consts {
        static let fontSize: CGFloat = 13
    }
    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = .gray
        return view
    }()
    private lazy var textView: UITextView = {
        let view = UITextView()
        view.isEditable = false
        view.font = .systemFont(ofSize: Consts.fontSize)
        view.textColor = .black
        view.alwaysBounceVertical = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageView)
        addSubview(textView)
        self.backgroundColor = .white;
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unavailable")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = CGRect(x: 0, y: 0,
                                 width: frame.width, height: frame.width / (ratio > 0 ? ratio : 1))
        let textSize = textView.sizeThatFits(CGSize(width: frame.width, height: CGFloat.greatestFiniteMagnitude))
        textView.frame = CGRect(x:0, y: imageView.frame.maxY,
                                width: frame.width, height: textSize.height)
        updateContentSize()
    }
    
    private func updateContentSize() {
        contentSize.height = textView.frame.maxY
    }
    
    var ratio: CGFloat = 1 {
        didSet {
            setNeedsLayout()
        }
    }
    
    var image: UIImage? {
        get {
            imageView.image
        }
        set {
            imageView.image = newValue
        }
    }
    
    var text: String {
        get {
            textView.text
        }
        set {
            textView.text = newValue
            setNeedsLayout()
        }
    }
}
