//
//  CatsListDataStore.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

enum CatsListItem {
    case item(CatUIModel, UIImage?)
    case loading
}

protocol CatsListDataSource {
    var items: [CatsListItem] { get }
}

protocol CatsListDataSourceDelegate: class {
    func dataSourceHasNewData(_ dataSource: CatsListDataSource, onlyImages: Bool)
}

class CatsListDataStore: CatsListDataSource {
    weak var delegate: CatsListDataSourceDelegate?
    var items: [CatsListItem] = [.loading]
    private var models: [CatUIModel] = []
    private var images: [String: UIImage] = [:]
    
    func updateModels(models: [CatUIModel]) {
        assertNotMainThread()
        
        self.models = models
        self.updateItems()
    }
    
    func updateImage(for id: String, with image: UIImage?) {
        assertNotMainThread()
        
        guard let image = image else { return }
        images[id] = image
        self.updateItems(onlyImages: true)
    }
    
    private func updateItems(onlyImages: Bool = false) {
        assertNotMainThread()
        
        var items: [CatsListItem] = models.map({ [images] in .item($0, images[$0.id]) })
        items.append(.loading)
        self.items = items
        notifyDelegate(onlyImages: onlyImages)
    }
    
    private func notifyDelegate(onlyImages: Bool) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.delegate?.dataSourceHasNewData(self, onlyImages: onlyImages)
        }
    }
}
