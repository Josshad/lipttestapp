//
//  CatsDetailsDataStore.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

protocol CatsDetailsDataSource {
    var model: CatUIModel { get }
    var text: String { get }
    var image: UIImage? { get }
}

protocol CatsDetailsDataSourceDelegate: class {
    func dataSourceHasNewData(_ dataSource: CatsDetailsDataSource)
}

class CatsDetailsDataStore: CatsDetailsDataSource {
    weak var delegate: CatsDetailsDataSourceDelegate?
    let model: CatUIModel
    let text: String
    private(set) var image: UIImage?
    init(model: CatUIModel, text: String) {
        self.model = model
        self.text = text
    }
    
    func update(image: UIImage) {
        assertMainThread()
        self.image = image
        notifyDelegate()
    }
    
    private func notifyDelegate() {
        assertMainThread()
        self.delegate?.dataSourceHasNewData(self)
    }
}
