//
//  CatsFlowController.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

class CatsFlowController {
    enum ScreenId {
        case list
        case details(CatModel)
    }
    
    private let assembly: CatsListAssembly
    private weak var rootViewController: CatsRootViewController?
    
    init(assembly: CatsListAssembly) {
        self.assembly = assembly
    }
    
    var rootController: CatsRootViewController {
        if let controller = rootViewController {
            return controller
        } else {
            let initialController = self.assembly.instantiateController(with: .list, flowController: self)
            let controller = CatsRootViewController(rootViewController: initialController)
            self.rootViewController = controller
            return controller
        }
    }
    
    func showScreen(_ screenId: ScreenId) {
        assertMainThread()
        let controller = self.assembly.instantiateController(with: screenId, flowController: self)
        rootViewController?.pushViewController(controller, animated: true)
    }
    
    func closeScreen() {
        assertMainThread()
        rootViewController?.popViewController(animated: true)
    }
}
