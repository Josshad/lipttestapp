//
//  CatsListViewController.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

class CatsListViewController: UIViewController {
    
    private enum Strings {
        static let title = "List"
    }
    
    var interactor: CatsListInteractor!
    var dataSource: CatsListDataSource!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unavailable")
    }
    
    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero,
                                  collectionViewLayout: layout)
        cv.alwaysBounceVertical = true
        cv.backgroundColor = .white
        cv.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        cv.register(CatsCell.self)
        cv.register(LoadingCell.self)
        return cv
    }()
    
    override func loadView() {
        view = collectionView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = Strings.title
        collectionView.dataSource = self
        collectionView.delegate = self
        interactor.perform(.initialFetch)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateVisibleCells()
    }
}

extension CatsListViewController: CatsListDataSourceDelegate {
    func dataSourceHasNewData(_ dataSource: CatsListDataSource, onlyImages: Bool) {
        if onlyImages {
            updateVisibleCells()
        } else {
            collectionView.reloadData()
        }
    }
    
    private func updateVisibleCells() {
        for indexPath in collectionView.indexPathsForVisibleItems {
            guard indexPath.item < dataSource.items.count else { return }
            let item = dataSource.items[indexPath.item]
            if case .item(_, let image) = item,
                let cell = collectionView.cellForItem(at: indexPath) as? CatsCell {
                cell.image = image
            }
        }
    }
}

extension CatsListViewController: UICollectionViewDataSource {
    func numberOfSections(in _: UICollectionView) -> Int {
        1
    }
    
    func collectionView(_ _: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        dataSource.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard indexPath.item < dataSource.items.count else {
            assertionFailure("Can't find item for indexpath \(indexPath)")
            return UICollectionViewCell()
        }
        
        
        let item = dataSource.items[indexPath.item]
        switch item {
        case .item(let model, let image):
            let cell = collectionView.dequeueReusableCell(CatsCell.self, for: indexPath)
            cell.text = "\(model.ratio)"
            cell.ratio = model.ratio
            cell.image = image
            return cell
        case .loading:
            let cell = collectionView.dequeueReusableCell(LoadingCell.self, for: indexPath)
            return cell
        }
    }
}

extension CatsListViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.frame.width
        return CGSize(width: width, height: CatsCell.prefferedHeight)
    }

    func collectionView(_ _: UICollectionView,
                        willDisplay _: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        guard indexPath.item < dataSource.items.count else {
            return
        }
        let item = dataSource.items[indexPath.item]
        switch item {
        case .item(let model, nil):
            interactor.perform(.loadImage(model))
        case .loading:
            interactor.perform(.loadNextPage)
        default: ()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        didEndDisplaying cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        guard indexPath.item < dataSource.items.count else {
            return
        }
        if case .item(let model, nil) = dataSource.items[indexPath.item] {
            interactor.perform(.cancelLoad(model.id))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.item < dataSource.items.count else {
            return
        }
        if case .item(let model, _) = dataSource.items[indexPath.item] {
            interactor.perform(.details(model.id))
        }
    }
}
