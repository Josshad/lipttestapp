//
//  CatsDetailsViewController.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

class CatsDetailsViewController: UIViewController {
    
    private enum Strings {
        static let title = "Title"
        static let back = "Back"
    }
    
    var interactor: CatsDetailsInteractor!
    var dataSource: CatsDetailsDataSource!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unavailable")
    }
    
    private let detailsView = DetailsView()
    
    override func loadView() {
        view = detailsView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let barBack = UIBarButtonItem(title: Strings.back, style: .plain, target: self, action: #selector(close))
        self.navigationItem.leftBarButtonItem = barBack
        self.navigationItem.title = Strings.title
        interactor.perform(.loadImage)
        updateUI()
    }
    
    private func updateUI() {
        detailsView.image = dataSource.image
        detailsView.ratio = dataSource.model.ratio
        detailsView.text = dataSource.text
    }
    
    @objc func close() {
        interactor.perform(.close)
    }
}

extension CatsDetailsViewController: CatsDetailsDataSourceDelegate {
    func dataSourceHasNewData(_ _: CatsDetailsDataSource) {
        updateUI()
    }
}
