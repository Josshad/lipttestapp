//
//  CatsListAssembly.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

class CatsListAssembly: FlowAssembly {
    private let adapter: CatsAdapter
    init(_ adapter: CatsAdapter) {
        self.adapter = adapter
    }
    
    func instantiateRootViewController() -> UIViewController {
        let flowController = CatsFlowController(assembly: self)
        let controller = flowController.rootController
        controller.flowController = flowController
        return controller
    }
    
    func instantiateController(with identifier: CatsFlowController.ScreenId,
                               flowController: CatsFlowController) -> UIViewController {
        switch identifier {
        case .list:
            let controller = CatsListViewController()
            let dataStore = CatsListDataStore()
            let interactor = CatsListInteractor(dataStore: dataStore,
                                                flowController: flowController,
                                                adapter: adapter)
            dataStore.delegate = controller
            controller.dataSource = dataStore
            controller.interactor = interactor
            return controller
        case .details(let model):
            let controller = CatsDetailsViewController()
            let dataStore = CatsDetailsDataStore(model: model.asUIModel(),
                                                 text: model.prettyDescription)
            let interactor = CatsDetailsInteractor(dataStore: dataStore,
                                                   flowController: flowController,
                                                   adapter: adapter)
            dataStore.delegate = controller
            controller.dataSource = dataStore
            controller.interactor = interactor
            return controller
        }
    }
}
