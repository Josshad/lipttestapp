//
//  CatModel+UI.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

extension CatModel {
    func asUIModel() -> CatUIModel {
        CatUIModel(id: id, ratio: ratio)
    }
    
    var prettyDescription: String {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        if let data = try? encoder.encode(self) {
            return String(data: data, encoding: .utf8) ?? ""
        }
        return ""
    }
    
    var ratio: CGFloat {
        height > 0 ? CGFloat(width)/CGFloat(height) : 0
    }
}
