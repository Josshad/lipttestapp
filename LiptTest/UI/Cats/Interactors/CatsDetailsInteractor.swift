//
//  CatsDetailsInteractor.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

struct CatsDetailsInteractor {
    enum Action {
        case close
        case loadImage
    }
    
    private let adapter: CatsAdapter
    private weak var dataStore: CatsDetailsDataStore?
    private weak var flowController: CatsFlowController?
    init(dataStore: CatsDetailsDataStore,
         flowController: CatsFlowController,
         adapter: CatsAdapter) {
        self.dataStore = dataStore
        self.flowController = flowController
        self.adapter = adapter
    }
    
    func perform(_ action: Action) {
        switch action {
        case .loadImage:
            guard let dataStore = dataStore,
                dataStore.image == nil else { return }
            let id = dataStore.model.id
            adapter.loadImageFor(id: id) { [dataStore] url in
                guard let url = url,
                    url.isFileURL,
                    let image = UIImage.imageWithoutOrientation(from: url) else {
                    return
                }
                DispatchQueue.main.async {
                    dataStore.update(image: image)
                }
            }
        case .close: flowController?.closeScreen()
        }
    }
}
