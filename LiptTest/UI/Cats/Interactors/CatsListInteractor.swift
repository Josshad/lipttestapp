//
//  CatsListInteractor.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

struct CatsListInteractor {
    enum Action {
        case initialFetch
        case loadImage(CatUIModel)
        case cancelLoad(String)
        case details(String)
        case loadNextPage
    }
    
    private enum Consts {
        static let maxHeight: CGFloat = 50
    }
    
    private var adapter: CatsAdapter
    private weak var dataStore: CatsListDataStore?
    private weak var flowController: CatsFlowController?
    private let updateQueue = DispatchQueue(label: "cats.list.datastore.update.serial")
    init(dataStore: CatsListDataStore,
         flowController: CatsFlowController,
         adapter: CatsAdapter) {
        self.dataStore = dataStore
        self.flowController = flowController
        self.adapter = adapter
    }
    
    func perform(_ action: Action) {
        switch action {
        case .initialFetch, .loadNextPage:
            adapter.loadNextPage({ [dataStore, adapter, updateQueue] error in
                updateQueue.async {
                    guard let dataStore = dataStore else { return }
                    let models = adapter.catsModels.map({ $0.asUIModel() })
                    dataStore.updateModels(models: models)
                }
            })
        case .loadImage(let model):
            adapter.loadImageFor(id: model.id) { [updateQueue, dataStore] url in
                updateQueue.async {
                    if let url = url,
                        url.isFileURL {
                        let size = CGSize(width: Consts.maxHeight * model.ratio, height: Consts.maxHeight)
                        let image = UIImage.resizedImage(from: url, for: size)
                        dataStore?.updateImage(for: model.id, with: image)
                    }
                }
            }
        case .cancelLoad(let id):
            adapter.cancelDownloadImage(id: id)
        case .details(let id):
            guard let model = adapter.catsModels.first(where: {$0.id == id}) else { return }
            flowController?.showScreen(.details(model))
        }
    }
}
