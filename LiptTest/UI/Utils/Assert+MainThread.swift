//
//  Assert+MainThread.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

@inlinable
func assertMainThread() {
    assert(Thread.isMainThread, "Should be called on main thread only")
}

@inlinable
func assertNotMainThread() {
    assert(!Thread.isMainThread, "Should not be called on main thread")
}
