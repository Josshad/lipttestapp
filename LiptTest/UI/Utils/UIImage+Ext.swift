//
//  Resizer.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import ImageIO
import UIKit

extension UIImage {
    static func resizedImage(from url: URL, for size: CGSize) -> UIImage? {
        let options: [CFString: Any] = [
            kCGImageSourceCreateThumbnailFromImageIfAbsent: true,
            kCGImageSourceCreateThumbnailWithTransform: false,
            kCGImageSourceShouldCacheImmediately: true,
            kCGImageSourceThumbnailMaxPixelSize: max(size.width, size.height)
        ]
        
        guard let imageSource = CGImageSourceCreateWithURL(url as NSURL, nil),
            let image = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, options as CFDictionary)
            else {
                return nil
        }
        
        return UIImage(cgImage: image)
    }
    
    static func imageWithoutOrientation(from url: URL) -> UIImage? {
        guard let imageSource = CGImageSourceCreateWithURL(url as NSURL, nil),
            let image = CGImageSourceCreateImageAtIndex(imageSource, 0, nil) else {
                return nil
        }
        return UIImage(cgImage: image, scale: 1, orientation: .up)
    }
}
