//
//  ServiceProvider.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

class ServiceProvider {
    private let httpClient: HTTPClient
    private let downloadClient: HTTPDownloadClient
    init(httpClient: HTTPClient = HTTPClientImpl(),
         downloadClient: HTTPDownloadClient = HTTPDownloadClientImpl()) {
        self.httpClient = httpClient
        self.downloadClient = downloadClient
    }
    
    lazy var catsService = CatsServiceImpl(httpClient: httpClient)
    lazy var downloadService = DownloadService(httpClient: downloadClient)
}
