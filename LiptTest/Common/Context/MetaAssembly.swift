//
//  MetaAssembly.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

struct MetaAssembly {
    private let hexProvider: HexProvider
    init(hexProvider: HexProvider = HexProvider()) {
        self.hexProvider = hexProvider
    }
    
    func catsAssembly() -> FlowAssembly {
        let adapter = CatsAdapterImpl(catsHex: hexProvider.catsHex,
                                      cacheHex: hexProvider.cacheHex)
        let assembly = CatsListAssembly(adapter)
        return assembly
    }
}
