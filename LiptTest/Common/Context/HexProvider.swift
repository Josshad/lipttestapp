//
//  HexProvider.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

class HexProvider {
    private let serviceProvider = ServiceProvider()
    private weak var weakCatsHex: CatsHexImpl?
    private weak var weakFileCacheHex: FileCacheHexImpl?
    
    var catsHex: CatsHex {
        if let hex = weakCatsHex {
            return hex
        } else {
            let hex = CatsHexImpl(service: serviceProvider.catsService,
                                  downloadService: serviceProvider.downloadService)
            weakCatsHex = hex
            return hex
        }
    }
    
    var cacheHex: CacheHex {
        if let hex = weakFileCacheHex {
            return hex
        } else {
            let hex = FileCacheHexImpl()
            weakFileCacheHex = hex
            return hex
        }
    }
}
