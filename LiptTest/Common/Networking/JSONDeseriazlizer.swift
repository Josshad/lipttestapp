//
//  JSONDeseriazlizer.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

public protocol Deserializer {
    func model<T>(from data: Data) -> T? where T: Decodable
}

struct JSONDeseriazlizer: Deserializer {
    static let `default` = JSONDeseriazlizer()
    private let decoder: JSONDecoder = JSONDecoder()

    func model<T>(from data: Data) -> T? where T: Decodable {
        try? decoder.decode(T.self, from: data)
    }
    
}
