//
//  HTTPClient.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

protocol HTTPClient {
    func getRequest<T: Decodable>(request: Request, completion: @escaping (T?, Error?) -> Void)
}

class HTTPClientImpl: HTTPClient {
    private enum Consts {
        static let baseURL = "https://api.thecatapi.com/v1/"
        static let authKey = "baca6eb0-38ae-432f-8356-d535ed8b4350"
    }
    
    private enum Headers {
        static let apiKey = "x-api-key"
    }
    
    private let session = URLSession(configuration: .ephemeral)
    private let deserializer = JSONDeseriazlizer.default
    
    func getRequest<T: Decodable>(request: Request, completion: @escaping (T?, Error?) -> Void) {
        let url = URL(string: Consts.baseURL + request.path)
        var request = URLRequest(url: url!)
        request.setValue(Consts.authKey, forHTTPHeaderField: Headers.apiKey)
        session.dataTask(with: request, completionHandler: { [weak self] (data, _, error) in
            if let error = error {
                completion(nil, error)
            }
            
            if let data = data,
                let model: T = self?.deserializer.model(from: data) {
                    completion(model, nil)
            } else {
                completion(nil, NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse))
            }
        }).resume()
    }
}

protocol HTTPDownloadClient {
    func download(url: URL, completion: @escaping (URL?, Error?) -> Void) -> RequestHandle
}

class HTTPDownloadClientImpl: HTTPDownloadClient {
    private let session: URLSession = {
        var configuration: URLSessionConfiguration = .ephemeral
        configuration.httpMaximumConnectionsPerHost = 2
        return URLSession(configuration: configuration)
    }()
    
    func download(url: URL, completion: @escaping (URL?, Error?) -> Void) -> RequestHandle {
        let task = session.downloadTask(with: url) { (urlToCache, _, error) in
            completion(urlToCache, error)
        }
        task.resume()
        return RequestHandle(task: task)
    }
}

