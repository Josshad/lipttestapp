//
//  RequestHandle.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

class RequestHandle {
    private weak var task: URLSessionTask?
    init(task: URLSessionTask) {
        self.task = task
    }
    
    func cancel() {
        self.task?.cancel()
    }
}
