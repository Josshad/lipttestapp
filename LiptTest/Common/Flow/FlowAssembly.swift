//
//  GenericAssembly.swift
//  LiptTest
//
//  Created by Danila Gusev on 13.09.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

protocol FlowAssembly {
    func instantiateRootViewController() -> UIViewController
}
